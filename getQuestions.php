<?php
    $conn = new mysqli("localhost", "root", "", "Quiz");  
    if ($conn->connect_error) {
        die("error");
    }

    $result = $conn->query("SELECT * FROM `Questions`");
    
    $i = 0;
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $i += 1;
            echo $row["Id"] . ':' . $row["Text"];

            if ($i != $result->num_rows) {
                echo ",";
            }
        }
    }

    echo "<-split->";

    $result = $conn->query("SELECT * FROM `Options`");
    
    $i = 0;
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $i += 1;
            echo $row["Id"] . ':' . $row["QuestionId"] . ":" . $row["Text"];

            if ($i != $result->num_rows) {
                echo ",";
            }
        }
    }

    $conn->close();
?>