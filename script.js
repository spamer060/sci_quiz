const quiz = document.getElementById("mainForm")

function buildSite(structure) {
    html = ""
    console.log(structure)

    for (const i of Object.keys(structure)) {
        const v = structure[i]
        html += `<h3> ${v.Question} </h3>`
        
        for (const j in v.Options) {
            console.log(v.Options[j])
            html += `<input type='radio' name="${i}" value="${v.Options[j][0]}"> ${v.Options[j][2]} <br>`
        }


    }
    
    html += '<br><br><input type="submit">';
    quiz.innerHTML = html
}

var xhr = new XMLHttpRequest()

xhr.onreadystatechange = function () {
    if (xhr.readyState == 4 && xhr.status == 200) {
        const raw = xhr.responseText
        const split = raw.split("<-split->")
        var anwsers = split[0].split(",")
        var options = split[1].split(",")

        const secondParser = (a) => {return a.split(":")};
        anwsers = anwsers.map(secondParser);
        options = options.map(secondParser);

        const survey = {}

        for (const v of anwsers) {
            survey[v[0]] = {Question: v[1], 
                Options: options.filter((e) => {return e[1] == v[0]})
            };
        }

        buildSite(survey);
    }
};

xhr.open("GET", "http://localhost/sci_quiz/getQuestions.php")
xhr.send()